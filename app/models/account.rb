class Account < ApplicationRecord
  has_many :announcements
  has_many :apps
  has_many :attachments
  has_many :brands
  has_many :business_hours
  has_many :canned_response_categories
  has_many :canned_responses
  has_many :companies
end
