module TrakDesk
  class Client < Request
    attr_accessor :api_url, :api_key, :path

    def initialize(params = {})
      @api_url = Rails.application.credentials.trakdesk[:api_url]
      @api_key = Rails.application.credentials.trakdesk[:api_key]
      @path = params[:path]
    end

    private

    def request_url(path)
      "#{api_url}#{path}"
    end

    def headers
      {}
    end

    def payload(params)
      {
        u: api_key
      }
    end
  end
end
