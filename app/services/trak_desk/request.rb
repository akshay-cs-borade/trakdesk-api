module TrakDesk
  class Request
    def get(params = {})
        data = RestClient.get(request_url(path), payload(params))
      rescue StandardError => e
        { "status" => e.http_code, "Message" => e.message }
    end
  end
end
