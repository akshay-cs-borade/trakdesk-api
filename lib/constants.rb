# Constants module to hold all constants
module Constants
  # Display Debug backtrace
  # If set to TRUE, a backtrace will be displayed along with php errors. If
  # error_reporting is disabled, the backtrace will not display, regardless of this setting

  SHOW_DEBUG_BACKTRACE = true;

  # AWS Service credentials

  CONF_S3_KEY = 'AKIAIVTCNWH3377U6LWQ';
  CONF_S3_SECRET = 'UFwdKbHo/pFi/E2A/S7cLD9XZkw0nPyE1LzdJTi2';


  # AWS s3 data path
  # This is the root of the user helpdesk path
  # Example: data/helpdesk/hash/

  CONF_S3_DATA_ROOT_PATH = 'data/helpdesk/';
  CONF_S3_BUCKET_PATH = 's3.trakdesk.com';
  CONF_S3_REGION = 'us-east-1';


  # AWS s3 GET path
  # This is used to get images/attachments etc.. stored on s3

  CONF_S3_GET_PATH = 'https://s3.amazonaws.com/s3.trakdesk.com/';


  # AWS s3 POST path
  # This is used to POST images/attachments etc.. to s3

  CONF_S3_POST_PATH = 's3://s3.trakdesk.com/';


  # App production root
  # This contains the app production root url to cloudfront
  # Note: This is also set in ember-app APP_PROD_URL

  CONF_APP_CF_PROD_URL = 'https://d3imn5kmlpzv7o.cloudfront.net';


  # App assets URL
  # This contains the assets url to cloudfront

  CONF_APP_ASSETS_URL =  "#{CONF_APP_CF_PROD_URL}/assets/";

  # AWS cloudfront production images url
  # Note: This constant is also set in trakdesk console .env file

  CONF_CF_PROD_IMAGES_PATH = 'https://dpqzjpym1hpca.cloudfront.net/production/images/';

  # Aws cloudfront marketplace themes path
  # Note: This constant is also set in trakdesk console .env file

  CONF_CF_THEMES_MARKETPLACE_PATH = 'https://d22rcj056f7j07.cloudfront.net/themes/';

  # Rollbar access token
  # This contains rollbar access token

  CONF_ROLLBAR_ACCESS_TOKEN = 'd5128b8fa1f348d4a837706c29db7a77';

  # Global configurations
  # This contains a list of global configurations

  CONF_TIME_ZONE = 'America/New_York';
  CONF_DATE_FORMAT = 'M d, Y';
  CONF_TIME_FORMAT = 'h:i A';

  # Current date and time
  # define('CONF_DATE', date('Y-m-d'));
  # define('CONF_TIME', date('H:i:s'));
  # // This contains DB date format for DATE_FORMAT() function
  # define('CONF_DB_DATETIME_FORMAT', "'%Y-%m-%d %H:%i'");
  # // Note: this is also used as database timestamps. DO NOT CHANGE FORMAT
  # define('CONF_DATETIME', date('Y-m-d H:i:s'));
  # define('CONF_DATETIME_UTC', gmdate('Y-m-d\TH:i:s\Z', strtotime(CONF_DATETIME)));

  # Support email
  # This contains trakdesk support email address

  CONF_SUPPORT_EMAIL = 'support@trakdesk.com';


  # Fields delimiter
  # This serves as a delimiter between a field array/alias and the field
  # Example: custom_fields.c55

  CONF_FIELDS_DELIMITER = '.';

  # Protocol
  # This contains the default HTTP protocol

  CONF_PROTO = 'https://';

  # Search min limit
  # This is the minimum number of characters required for searching

  CONF_SEARCH_MIN_LENGTH = 1;

  # Search max limit
  # This is the maximum number of characters allowed for searching

  CONF_SEARCH_MAX_LENGTH = 150;

  # Pagination per page
  # This contains default objects per page

  CONF_PAGINATION_PER_PAGE = 30;

  # Pagination max limit per page
  # This contains the maximum objects per page

  CONF_PAGINATION_MAX_PER_PAGE = 50;

  # Bulk operations limit
  # This applies to both DELETE/PUT methods
  # Note: Ideally, this value should be same as the max pagination limit

  CONF_BULK_OPERATIONS_LIMIT = CONF_PAGINATION_MAX_PER_PAGE;

  # Email address character limit

  CONF_EMAIL_ADDRESS_CHAR_LIMIT = 150;

  # Payload limit
  # This contains the current payload limit. Default is 5MB

  CONF_PAYLOAD_LIMIT = 5000000;

  # Max file size
  # This contains the default maximum file limit allowed
  # This is updated from the helpdesk config

  CONF_MAX_FILE_SIZE = 5000000;

  # This contains the max number of files that is
  # allowed to be upload in one request

  CONF_MAX_FILES = 10;

  # Max includes
  # This contains the maximum number of includes allowed in the API request
  # Note: This restriction does not apply for webapp

  CONF_MAX_REQUEST_INCLUDES = 3;

  # Max select fields
  # This contains the maximum number of select fields in the API request
  # Note: This restriction does not apply for webapp

  CONF_MAX_REQUEST_FIELDS = 25;

  # Sorting options
  # This contains a list of default sorting options
  # Note: do not remove 'name' attribute from here as its being used many places

  CONF_SORTING_FIELDS = [
      'id', 'name', 'created_at', 'updated_at'
  ];

  # Encrypt password key
  # This key is used to encrypt plain text in database
  # Such as: custom SMTP server password etc..

  CONF_ENCRYPT_PASSWORD_KEY = 'XlxBe0e';

  # Response 'code' constants
  # A list of response code constants

  REQUIRED_FIELD = 'required_field';
  INVALID_FIELD = 'invalid_field';
  INVALID_VALUE = 'invalid_value';
  READONLY_FIELD = 'readonly_field';
  DUPLICATE_VALUE = 'duplicate_value';
  DATATYPE_MISMATCH = 'datatype_mismatch';
  INVALID_JSON = 'invalid_json';
  NOT_FOUND = 'not_found';
  FEATURE_ACCESS = 'feature_access';
  ACCESS_DENIED = 'access_denied';
  MAX_RECORD_LIMIT = 'max_record_limit';
  SSL_REQUIRED = 'ssl_required';
  INVALID_URL = 'invalid_url';
  METHOD_NOT_ALLOWED = 'method_not_allowed';
  INVALID_CREDENTIALS = 'invalid_credentials';
  ACCOUNT_NOT_FOUND = 'account_not_found';
  RATE_LIMIT_REACHED = 'rate_limit_reached';
  ACCOUNT_STATE = 'account_state';
  NO_CONTENT_REQUIRED = 'no_content_required';
  OPERATION_NOT_ALLOWED = 'operation_not_allowed';
  INVALID_PARAMETER = 'invalid_parameter';
  PAYMENT_PROCESSOR = 'payment_processor';
  INTERNAL_ERROR = 'internal_error';
  MISSING_FIELD = 'missing_field';
  EXCEED_VALUE = 'exceed_value';
  LOCKED_FIELD = 'locked_field';
  NOT_MODIFIED = 'not_modified';

  # Error responses
  # This contains a list of error responses

  MSG_ACCESS_DENIED = 'Insufficient privileges to perform this action';
  MSG_REQUIRED_FIELD = 'A required field value is missing';
  MSG_INVALID_VALUE = 'An invalid value was part of this request';
  MSG_INVALID_VALUE_SUGGESTIONS = 'An invalid value was part of this request. It should be one of these value(s): ';
  MSG_INVALID_EMAIL = 'An invalid email address was part of this request';

  # Before changing the wording on these, please check all places where it has been used
  MSG_DATATYPE_MISMATCH = 'The value set is of type [given]. It should be a/an [expected]';
  MSG_DATATYPE_MISMATCH_EMAIL = 'Value should be of type valid Email address(es)';
  MSG_DATATYPE_MISMATCH_INTEGER = 'Value should be of type Integer(s)';
  MSG_DATATYPE_MISMATCH_ARRAY_INTEGER = 'Value should be of type Array of Integer(s)';
  MSG_DATATYPE_MISMATCH_STRING = 'Value should be of type String(s)';
  MSG_DATATYPE_MISMATCH_ARRAY_STRING = 'Value should be of type Array of String(s)';
  MSG_NO_MATCHING_ID = 'No result matching the id';
  MSG_NO_MATCHING_IDS = 'No results matching these id(s): ';
  MSG_INVALID_FIELD = 'An invalid field was part of this request';

  # Used in models to throw errors about invalid fields for json/array data
  MSG_INVALID_FIELDS = 'Your request contains invalid fields(s): ';
  MSG_EXCEED_VALUE = 'A field value exceeds the expected value';
  MSG_SSL_REQUIRED = 'HTTPS is required to access this API';
  MSG_READONLY_FIELD = 'Read-only field cannot be altered';
  MSG_LOCKED_FIELD = 'Agent is not allowed to access this field';
  MSG_INVALID_JSON = 'Your request contains invalid JSON content';
  MSG_NO_CONTENT_REQUIRED = 'No content is required for this request';
  MSG_FEATURE_ACCESS = 'Account does not have access to this feature';
  MSG_FEATURE_ACCESS_UPGRADE = 'Upgrade your account to access this feature';
  MSG_APP_NOT_INSTALLED = 'App is not installed';
  MSG_DUPLICATE_VALUE = 'A duplicate value was part of this request';
  MSG_UNDEFINED_VALUE = 'A required object value is undefined';
  MSG_ACCOUNT_STATE = 'Your account is currently in an inactive state';
  MSG_CONTACT_US = 'Please contact us at support@trakdesk.com for more details';
  MSG_VALUE_FORMATS = 'Value should be in one of these format(s): ';

  # Responses
  MSG_NOT_FOUND = 'Resource not found';
  MSG_RATE_LIMIT_REACHED = 'Rate Limit exceeded';
  MSG_MAX_RECORD_LIMIT = 'Maximum record limit reached';
  MSG_METHOD_NOT_ALLOWED = 'Method not allowed';
  MSG_METHOD_NOT_ALLOWED_SUGGESTIONS = 'Method not allowed. It should be one of these method(s): ';
  MSG_OPERATION_NOT_ALLOWED = 'Method not allowed';
  MSG_INVALID_CREDENTIALS = 'Incorrect or missing API credentials';
  MSG_ACCOUNT_NOT_FOUND = 'Helpdesk account not found';
  MSG_INVALID_PARAMETER = 'An invalid parameter was part of this request';
  MSG_INVALID_URL = 'You have entered an invalid endpoint URL';
  MSG_INTERNAL_ERROR = 'Woops!, something went terribly wrong. Please contact us at support@trakdesk.com for more details';
  MSG_MAX_RECORDS = 'You can add up to a maximum of %s';
  MSG_MAX_CHARACTERS = '%s should contain no more than %s characters';
end
